import java.util.Scanner;
public class Tarsashoz {
    public static void main(String [] args) {
        Scanner sc = new Scanner(System.in);
        int kockaszam = sc.nextInt();
        Dice kocka = new Dice(6);
        int osszeg = 0;

        for (int i = 0; i < kockaszam; i++) {
            int x = kocka.throwIt();
            osszeg = osszeg + x;
            System.out.print(x + " ");
        }
        System.out.println();
        System.out.println("osszeg = " + osszeg);
    }


}
